%global gem_name tzinfo
Name:                rubygem-%{gem_name}
Version:             2.0.6
Release:             1
Summary:             Daylight savings aware timezone library
License:             MIT
URL:                 http://tzinfo.github.io
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1:             %{gem_name}-%{version}-tests.txz
BuildRequires:       ruby(release) rubygems-devel ruby rubygem(minitest) rubygem(concurrent-ruby)
BuildRequires:       rubygem(did_you_mean)
BuildArch:           noarch
%description
TZInfo provides daylight savings aware transformations between times in
different time zones.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ln -s %{_builddir}/test .

# We don't want to use bundler
sed -i "/raise 'Tests must be run with bundler/ s/^/#/" \
  test/test_utils.rb

export RUBYOPT="-Ilib"

ruby test/ts_all_ruby_format1.rb
ruby test/ts_all_ruby_format2.rb
ruby test/ts_all_zoneinfo.rb

# Test with system tzdata.
sed -i '/zoneinfo_path/ s|= .*|= "%{_datadir}/zoneinfo"|' test/ts_all_zoneinfo.rb

# The test is designed to run with internal zoneinfo fixtures, therefore there
# might be test failures.
# https://github.com/tzinfo/tzinfo/issues/141
ruby test/ts_all_zoneinfo.rb || :
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.yardopts
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGES.md
%doc %{gem_instdir}/README.md

%changelog
* Thu Aug 10 2023 wubijie <wubijie@kylinos.cn> - 2.0.6-1
- Upgrade to version 2.0.6

* Wed Mar 2 2022 liqiuyu <liqiuyu@kylinos.cn> - 2.0.4-1
- update to 2.0.4

* Mon Feb 21 2022 liyanan <liyanan32@huawei.com> - 1.2.5-3
- fix check failed for rubygem-timecop

* Sat Sep 5 2020 liyanan <liyanan32@huawei.com> - 1.2.5-2
- fix build fail

* Sat Aug 8 2020 yanan li <liyanan032@huawei.com> - 1.2.5-1
- Package init
